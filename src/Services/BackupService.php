<?php

namespace App\Services;

use MongoDB\Client;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;


class BackupService
{
    public function __construct(string $mongoUri, string $projectDir)
    {
        $this->mongoUri = $mongoUri;
        $this->projectDir = $projectDir;
        $this->tempDir = $projectDir . '/var/tmp';
        $this->client = new Client($mongoUri);
    }

    public function mongoBackup(string $dbName)
    {
        // Ensure the backup directory exists
        if (!is_dir($this->tempDir)) {
            if (!mkdir($this->tempDir, 0777, true) && !is_dir($this->tempDir)) {
                throw new \RuntimeException(sprintf('Directory "%s" was not created', $this->tempDir));
            }
        }

        $backupPath = $this->tempDir . '/database-' . $dbName . '-' . date('YmdHis') . '.gz';
        $command = [
            'mongodump',
            '--uri=' . $this->mongoUri . '/' . $dbName,
            //'--db=' . $dbName,
            '--archive=' . $backupPath,
            '--gzip'
        ];

        $process = new Process($command);
        $process->run();

        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }

        return $backupPath;
    }

    public function mongoRestore(string $dbName, string $tempBackupPath)
    {
        $command = [
            'mongorestore',
            '--uri=' . $this->mongoUri,
            '--nsInclude=' . $dbName . '.*', // Include all collections in the database
            '--archive=' . $tempBackupPath,
            '--gzip',
            '--drop'
        ];

        $process = new Process($command);
        $process->run();

        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }
    }

    public function getDatabaseSize(string $dbName): string
    {
        $db = $this->client->selectDatabase($dbName);
        $stats = $db->command(['dbStats' => 1])->toArray()[0];
        return $stats['storageSize'];
    }

    public function getUploadsSize(string $dbName): string
    {
        $directoryPath = $this->projectDir . '/web/uploads/' . $dbName;
        return $this->getFolderSize($directoryPath);
    }

    private function getFolderSize($directory): int
    {
        $size = 0;
        if (is_dir($directory)) {
            foreach (new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($directory, \FilesystemIterator::SKIP_DOTS)) as $file) {
                $size += $file->getSize();
            }
        }
        return $size;
    }

    public function formatBytes($bytes, $precision = 2)
    {
        $units = ['B', 'KB', 'MB', 'GB', 'TB'];

        $bytes = max($bytes, 0);
        $pow = floor(($bytes ? log($bytes) : 0) / log(1024));
        $pow = min($pow, count($units) - 1);

        $bytes /= (1 << (10 * $pow));

        return round($bytes, $precision) . ' ' . $units[$pow];
    }

    public function createGzipArchive(array $sourcePaths, string $dbName)
    {
        $tarFile = $this->tempDir . '/' . $dbName . '_backup' . '-' . date('YmdHis') . '.tar';
        $gzipFile = $tarFile . '.gz';

        // Delete existing files if they exist
        if (file_exists($tarFile)) {
            unlink($tarFile);
        }
        if (file_exists($gzipFile)) {
            unlink($gzipFile);
        }

        // Create tar archive
        $phar = new \PharData($tarFile);
        foreach ($sourcePaths as $sourcePath) {
            if (is_dir($sourcePath)) {
                $this->addDirectoryToTar($phar, $sourcePath, $sourcePath);
            } elseif (is_file($sourcePath)) {
                $relativePath = basename($sourcePath);
                $phar->addFile($sourcePath, $relativePath);
            }
        }
        $phar->compress(\Phar::GZ);

        // Delete the uncompressed tar file
        unlink($tarFile);

        return $gzipFile;
    }

    private function addDirectoryToTar(\PharData $phar, string $directory, string $baseDir)
    {
        $iterator = new \RecursiveIteratorIterator(
            new \RecursiveDirectoryIterator($directory, \RecursiveDirectoryIterator::SKIP_DOTS),
            \RecursiveIteratorIterator::SELF_FIRST
        );
    
        foreach ($iterator as $file) {
            $relativePath = substr($file->getPathname(), strlen($baseDir) + 1);
            $relativePath = ltrim($relativePath, DIRECTORY_SEPARATOR);  // Normalize the relative path
    
            // Skip files directly in the root folder
            if (dirname($relativePath) === '.') {
                continue;
            }
    
            if ($file->isDir()) {
                $phar->addEmptyDir($relativePath);
            } elseif ($file->isFile()) {
                $phar->addFile($file->getPathname(), $relativePath);
            }
        }
    }
}
