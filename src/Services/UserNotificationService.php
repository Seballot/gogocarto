<?php

namespace App\Services;

use App\Document\watchFrequencyOptions;
use Doctrine\ODM\MongoDB\DocumentManager;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class UserNotificationService
{
    protected $config;
    protected $locale;

    public function __construct(DocumentManager $dm, MailService $mailService,
                                UrlService $urlService, TranslatorInterface $t)
    {
        $this->dm = $dm;
        $this->mailService = $mailService;
        $this->urlService = $urlService;
        $this->t = $t;
    }

    private function getConfig()
    {
        if (!$this->config) $this->config = $this->dm->get('Configuration')->findConfiguration();
        return $this->config;
    }

    private function getLocale()
    {
        if (!$this->locale) $this->locale = $this->getConfig() ? $this->getConfig()->getLocale() : null;
        return $this->locale;
    }

    function trans($message, $args, $domain = 'messages') {
        return $this->t->trans($message, $args, $domain, $this->getLocale());
    }

    function getUserNotificationMailSubject($eventType) {
        $config = $this->getConfig();
        return $this->trans('notifications.' . $eventType . '.subject', [ 'appname' => $config->getAppName() ]);
    }

    function getUserNotificationMailContent($eventType, $items, $user, $nameField, $dateField) {
        $config = $this->getConfig();
        $list = '<ul>';
        foreach ($items as $itemData) {
            $editUrl = $this->urlService->generateUrlFor(
                $config->getDbName(),
                (isset($itemData['username'])) ? 'admin_app_user_edit' : 'admin_app_element_edit',
                ['id' => $itemData['id']]
            );
            $list .= '<li><a href="' . htmlspecialchars($editUrl) . '">' . htmlspecialchars($itemData[$nameField]) . '</a> - ' . htmlspecialchars($itemData[$dateField]) . '</li>';
        }
        $list .= '</ul>';
        return $this->trans('notifications.' . $eventType . '.content', [
            'count' => count($items),
            'element_singular' => $config->getElementDisplayName(),
            'element_plural' => $config->getElementDisplayNamePlural(),
            'appname' => $config->getAppName(),
            'url' => $this->urlService->generateUrlFor($config->getDbName(), 'gogo_directory'),
            'edit_url' => $this->urlService->generateUrlFor($config->getDbName(), 'admin_app_user_edit', ['id' => $user->getId()]),
            'list' => $list
        ]);
    }

    function buildAndSendEmailToUser($eventType, $items, $user, $nameField, $dateField) {
        if (count($items) > 0) {
            $subject = $this->getUserNotificationMailSubject($eventType);
            $content = $this->getUserNotificationMailContent($eventType, $items, $user, $nameField, $dateField);
            $this->mailService->sendMail($user->getEmail(), $subject, $content);
            return true;
        }
        return false;
    }

    function sendUsersNotifications()
    {
        $users = $this->dm->get('User')->findBywatch(true);
        $usersNotified = 0;
        $config = $this->getConfig();
        if (!$config) return;

        foreach ($users as $user) {

            $userNotified = false;
            $nextPeriod = $user->getNextUserNotificationPeriod();

            // Check if user has to be notified regarding to the frequency
            if ($nextPeriod) {
                $now = new \DateTime();
                switch($user->getWatchFrequency()) {
                    case watchFrequencyOptions::WEEKLY:
                        $currentPeriod = $now->format('Y-W');
                        break;
                    case watchFrequencyOptions::MONTHLY:
                        $currentPeriod = $now->format('Y-m');
                        break;
                    default:
                        $currentPeriod = $nextPeriod;
                }
                if ($currentPeriod < $nextPeriod) { 
                    continue;
                }
            }

            foreach($user->getWatchedEventsTypes() as $eventType) {

                switch($eventType) {

                    case 'moderation':
                        $elementsFound = $this->dm->get('Element')->findModerationElementToNotifyToUser($user);
                        $userNotified = $this->buildAndSendEmailToUser($eventType, $elementsFound, $user, 'name', 'updatedAt') || $userNotified;
                        break;

                    case 'element_added':
                        $elementsFound = $this->dm->get('Element')->findAddedElementToNotifyToUser($user);
                        $userNotified = $this->buildAndSendEmailToUser($eventType, $elementsFound, $user, 'name', 'createdAt') || $userNotified;
                        break;

                    case 'element_edited':
                        $elementsFound = $this->dm->get('Element')->findEditedElementToNotifyToUser($user);
                        $userNotified = $this->buildAndSendEmailToUser($eventType, $elementsFound, $user, 'name', 'updatedAt') || $userNotified;
                        break;

                    case 'element_deleted':
                        $elementsFound = $this->dm->get('Element')->findDeletedElementToNotifyToUser($user);
                        $userNotified = $this->buildAndSendEmailToUser($eventType, $elementsFound, $user, 'name', 'updatedAt') || $userNotified;
                        break;

                    case 'user_added':
                        $usersFound = $this->dm->get('User')->findAddedUsersToNotifyToUser($user);
                        $userNotified = $this->buildAndSendEmailToUser($eventType, $usersFound, $user, 'username', 'createdAt') || $userNotified;
                        break;
                }
            }

            if ($userNotified) {
                $usersNotified++;
                $user->setLastUserNotificationSentAt(new \Datetime());
            }
        }

        $this->dm->flush();
        return $usersNotified; 
    }

    function notifyImportError($import)
    {
        if (!$import->isDynamicImport()) return;
        foreach($import->getUsersToNotify() as $user) {
            $config = $this->dm->get('Configuration')->findConfiguration();
            $subject = $this->trans('notifications.import_error.subject', [ 'appname' => $config->getAppName() ]);
            $content = $this->trans('notifications.import_error.content', [ 
                'import' => $import->getSourceName(),
                'url' => $this->urlService->generateUrlFor($config, 'admin_app_import_edit', ['id' => $import->getId()])
            ]);
            $this->mailService->sendMail($user->getEmail(), $subject, $content);
        }
    }

    function notifyImportMapping($import)
    {
        if (!$import->isDynamicImport()) return;
        foreach($import->getUsersToNotify() as $user) {
            $config = $this->dm->get('Configuration')->findConfiguration();
            $subject = $this->trans('notifications.import_mapping.subject', [ 'appname' => $config->getAppName() ]);
            $content = $this->trans('notifications.import_mapping.content', [
                'import' => $import->getSourceName(),
                'url' => $this->urlService->generateUrlFor($config, 'admin_app_import_edit', ['id' => $import->getId()])
            ]);
            $this->mailService->sendMail($user->getEmail(), $subject, $content);
        }
    }
}