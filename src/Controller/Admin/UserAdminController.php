<?php

namespace App\Controller\Admin;

use App\Services\MailService;
use Doctrine\ODM\MongoDB\DocumentManager;
use Sonata\AdminBundle\Controller\CRUDController as Controller;
use Sonata\AdminBundle\Datagrid\ProxyQueryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\Translation\TranslatorInterface;

class UserAdminController extends Controller
{
    public function __construct(MailService $mailService, TranslatorInterface $t, DocumentManager $dm)
    {
        $this->mailService = $mailService;
        $this->t = $t;
        $this->dm = $dm;
    }

    public function batchActionSendMail(ProxyQueryInterface $selectedModelQuery)
    {
        $selectedModels = $selectedModelQuery->execute();
        $nbreModelsToProceed = $selectedModels->count();
        $selectedModels->limit(5000);

        $request = $this->get('request_stack')->getCurrentRequest()->request;

        $mails = [];
        $usersWithoutEmail = 0;

        try {
            foreach ($selectedModels as $user) {
                $mail = $user->getEmail();
                if ($mail) {
                    $mails[] = $mail;
                } else {
                    ++$usersWithoutEmail;
                }
            }
        } catch (\Exception $e) {
            $this->addFlash('sonata_flash_error', $this->trans('sonata.user.user.batch.error', [$e->getMessage()], 'admin'));

            return new RedirectResponse($this->admin->generateUrl('list', $this->admin->getFilterParameters()));
        }

        if (!$request->get('mail-subject') || !$request->get('mail-content')) {
            $this->addFlash('sonata_flash_error', $this->trans('sonata.user.user.batch.mailError', [], 'admin'));
        } elseif (count($mails) > 0) {
            $result = $this->mailService->sendMail(null, $request->get('mail-subject'), $request->get('mail-content'), $request->get('from'), $mails);
            if ($result['success']) {
                $this->addFlash('sonata_flash_success', $this->trans('sonata.user.user.batch.sendmails', ['%count%' => count($mails)], 'admin'));
            } else {
                $this->addFlash('sonata_flash_error', $result['message']);
            }
        }

        if ($usersWithoutEmail > 0) {
            $this->addFlash('sonata_flash_error', $this->trans('usersWithoutEmail', ['%count%' => $usersWithoutEmail], 'admin'));
        }

        $limit = 5000;
        if ($nbreModelsToProceed >= $limit) {
            $this->addFlash('sonata_flash_info', $this->trans('sonata.user.user.batch.tooMany', ['%limit%' => $limit], 'admin'));
        }

        return new RedirectResponse($this->admin->generateUrl('list', $this->admin->getFilterParameters()));
    }

    public function deleteAction($id): Response
    {
        $user = $this->admin->getObject($id);

        if (!$user) {
            throw $this->createNotFoundException(sprintf('Unable to find the object with id: %s', $id));
        }

        if (in_array('ROLE_SUPER_ADMIN', $user->getRoles())) {

            $superAdminCount = $this->dm->get('User')->countSuperAdminUsers();

            // If this is the last super admin, deny the deletion
            if ($superAdminCount <= 1) {
                $this->addFlash('error', $this->trans('sonata.user.user.error.deletingLastSuperAdmin'));
                return new RedirectResponse($this->admin->generateUrl('edit', ['id' => $id]));
            }
        }

        return parent::deleteAction($id);
    }
}
