<?php

namespace App\Controller\Admin;

use App\Form\MongoRestoreType;
use App\Helper\GoGoHelper;
use App\Services\BackupService;
use App\Services\DocumentManagerFactory;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController as Controller;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class MyProjectController extends Controller
{
    public function __construct(
        BackupService $backupService,
        DocumentManagerFactory $documentManagerFactory,
        KernelInterface $kernel,
        TranslatorInterface $t
    ) {
        $this->backupService = $backupService;
        $this->dbName = $documentManagerFactory->getCurrentDbName();
        $this->projectDir = $kernel->getProjectDir();
        $this->projectUploadsDir = $this->projectDir . '/web/uploads/' .  $this->dbName;
        $this->projectTempDir = $this->projectDir . '/var/tmp/';
        $this->t = $t;
    }


    public function delete(): Response
    {
        return $this->render('/admin/pages/my_project/my_project_delete.html.twig');
    }

    
    public function backupAndRestore(Request $request): Response
    {
        $restoreForm = $this->createForm(MongoRestoreType::class);
        $restoreForm->handleRequest($request);

        if ($restoreForm->isSubmitted() && $restoreForm->isValid()) {
            $this->restore($restoreForm);
        }

        $databaseSize = $this->backupService->getDatabaseSize($this->dbName);
        $uploadsSize = $this->backupService->getUploadsSize($this->dbName);
        $totalSize = $databaseSize + $uploadsSize;
        $databaseSize = $this->backupService->formatBytes($databaseSize);
        $uploadsSize = $this->backupService->formatBytes($uploadsSize);
        $totalSize = $this->backupService->formatBytes($totalSize);

        return $this->render('/admin/pages/my_project/my_project_backup_and_restore.html.twig', [
            'restoreForm' => $restoreForm->createView(),
            'databaseSize' => $databaseSize,
            'uploadsSize' => $uploadsSize,
            'totalSize' => $totalSize,
        ]);
    }

    public function backup(): Response
    {
        try {

            // Create mongobackup and store it temporarily
            $mongoBackupPath = $this->backupService->mongoBackup($this->dbName);

            // Create gzip archive with mongobackup and uploads and store it temporarily
            $backupPath = $this->backupService->createGzipArchive([$this->projectUploadsDir, $mongoBackupPath], $this->dbName);

            if (!$backupPath) {
                $this->addFlash('error', $this->t->trans('my_project.no_backup_file'));
                return $this->redirectToRoute('gogo_my_project_backup_and_restore');
            }

            $response = new StreamedResponse(function() use ($backupPath) {
                if (file_exists($backupPath)) {
                    readfile($backupPath);
                    unlink($backupPath); // Delete the file after sending it to the user
                } else {
                    throw new \RuntimeException(sprintf('File "%s" does not exist.', $backupPath));
                }
            });

            $response->headers->set('Content-Type', 'application/gzip');
            $response->headers->set('Content-Disposition', 'attachment; filename="' . basename($backupPath) . '"');

            return $response;

        } catch (\Exception $e) {
            $this->addFlash('error', $this->t->trans('my_project.backup_failed') . ' : ' . $e->getMessage());
            return $this->redirectToRoute('gogo_my_project_backup_and_restore');
        }
    }

    public function restore(Form $restoreForm)
    {
        $backupFile = $restoreForm->get('backupFile')->getData();

        if ($backupFile) {
            $newFilename = 'backup-' . uniqid() . '.' . $backupFile->guessExtension();

            try {
                // Move the file to the temporary directory
                $backupFile->move($this->projectTempDir, $newFilename);

                // Full path to the file
                $tempBackupPath = $this->projectTempDir . $newFilename;

                // Validate the contents of the backup file
                $this->validateBackupFile($tempBackupPath);

                // Clean the specific directory within /web/uploads
                $this->cleanUploadsDirectory($this->dbName);

                // Extract the backup file to /web/uploads
                $gzMongoFile = $this->extractBackup($tempBackupPath, $this->projectDir . '/web/uploads/' . $this->dbName);

                // Perform the restore for MongoDB
                $this->backupService->mongoRestore($this->dbName, $gzMongoFile);

                // Remove the uploaded backup file after restoring
                unlink($tempBackupPath);

                $this->addFlash('success', $this->t->trans('my_project.restore_succeed'));

            } catch (\Exception $e) {
                $this->addFlash('error', $this->t->trans('my_project.restore_failed') . ' : ' . $e->getMessage());
            }
        } else {
            $this->addFlash('error', $this->t->trans('my_project.no_backup_file'));
        }
    }

    private function validateBackupFile(string $archivePath): void
    {
        $phar = new \PharData($archivePath);

        $gzFiles = [];
        
        foreach ($phar as $file) {
            if ($file->isFile()) {
                $files[] = $file->getFilename();
                if ($file->getExtension() === 'gz') {
                    $gzFiles[] = $file->getFilename();
                }
            }
        }

        if (count($gzFiles) !== 1 || count($files) !== 1) {
            throw new \RuntimeException($this->t->trans('my_project.incorrect_backup_file.database', ['%dbName%' => $this->dbName]));
        }
    }

    private function cleanUploadsDirectory(string $dbName): void
    {
        $filesystem = new Filesystem();

        if ($filesystem->exists($this->projectUploadsDir)) {
            $filesystem->remove($this->projectUploadsDir);
        }
        $filesystem->mkdir($this->projectUploadsDir);
    }

    private function extractBackup(string $archivePath, string $destinationDir): string
    {
        $filesystem = new Filesystem();
        
        // Ensure the destination directory exists
        if (!$filesystem->exists($destinationDir)) {
            $filesystem->mkdir($destinationDir);
        }
    
        // Initialize the name of the `.gz` file to be used for MongoDB restore
        $gzFilePath = '';
    
        // Check if the file is compressed and handle accordingly
        if (strpos($archivePath, '.gz') !== false) {

            $tarFile = str_replace('.gz', '.tar', $archivePath);
            
            try {
                // Decompress the .tar.gz file to create a .tar file
                $phar = new \PharData($archivePath);
                $phar->decompress(); // Creates a .tar file
            } catch (\Exception $e) {
                throw new \RuntimeException($this->t->trans('my_project.incorrect_backup_file.decompression_failed') . ' (' . $e->getMessage() . ')');
            }

            // After decompression, extract the .tar file
            try {
                $phar = new \PharData($tarFile);
                $phar->extractTo($destinationDir, null, true); // Extract the content
            } catch (\Exception $e) {
                throw new \RuntimeException($this->t->trans('my_project.incorrect_backup_file.decompression_failed') . ' (' . $e->getMessage() . ')');
            }
   
            // Remove the .tar file after extraction
            $filesystem->remove($tarFile);

            // Find the correct .gz file for MongoDB restore
            $gzFiles = glob($destinationDir . '/database-' . $this->dbName . '*.gz');
            if (count($gzFiles) !== 1) {
                throw new \RuntimeException($this->t->trans('my_project.incorrect_backup_file.database', ["%dbName%" => $this->dbName]));
            }
    
            $gzFilePath = $gzFiles[0];

        } else {
            throw new \RuntimeException($this->t->trans('my_project.incorrect_backup_file.format', ["%dbName%" => $this->dbName]));
        }
    
        return $gzFilePath;
    }
}