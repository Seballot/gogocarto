<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Contracts\Translation\TranslatorInterface;

class MongoRestoreType extends AbstractType
{
    public function __construct(TranslatorInterface $t)
    {
        $this->t = $t;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('backupFile', FileType::class, [
                'label' => $this->t->trans('my_project.backup_and_restore.restore.input_label', [], 'admin'),
                'constraints' => [
                    new File([
                        'maxSize' => '1024M',
                        'maxSizeMessage' => 'The file is too large ({{ size }} {{ suffix }}). Allowed maximum size is {{ limit }} {{ suffix }}.',
                        'mimeTypes' => [
                            'application/gzip',
                            'application/x-gzip',
                        ],
                        'mimeTypesMessage' => 'Please upload a valid Gzip file',
                    ])
                ],
                'attr' => [
                    'accept' => '.gz,.gzip',
                ],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([]);
    }
}