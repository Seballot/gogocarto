<?php

namespace App\Command;

use App\Services\AsyncService;
use Doctrine\ODM\MongoDB\DocumentManager;
use App\Services\DocumentManagerFactory;
use App\Services\UserNotificationService;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class NotifyUsersCommand extends GoGoAbstractCommand
{
    public function __construct(DocumentManagerFactory $dm, LoggerInterface $commandsLogger,
                               TokenStorageInterface $security,
                               UserNotificationService $notifService,
                               TranslatorInterface $t,
                               AsyncService $as)
    {
        $this->notifService = $notifService;
        parent::__construct($dm, $commandsLogger, $security, $t, $as);
    }

    protected function gogoConfigure(): void
    {
        $this
        ->setName('app:users:notify')
        ->setDescription('Notify users about pending moderation, problem on an import or other events'); // 
    }

    protected function gogoExecute(DocumentManager $dm, InputInterface $input, OutputInterface $output): void
    {
        $userNotified = $this->notifService->sendUsersNotifications();
        if ($userNotified) {
            $this->log("Notify, $userNotified users notified"); // 
        }        
    }
}
