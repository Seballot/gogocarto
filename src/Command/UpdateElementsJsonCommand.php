<?php

namespace App\Command;

use App\EventListener\ElementJsonGenerator;
use App\Services\AsyncService;
use Doctrine\ODM\MongoDB\DocumentManager;
use App\Services\DocumentManagerFactory;
use Psr\Log\LoggerInterface;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class UpdateElementsJsonCommand extends GoGoAbstractCommand
{
    private FilesystemAdapter $cache;

    public function __construct(
        DocumentManagerFactory $dm,
        LoggerInterface $commandsLogger,
        TokenStorageInterface $security,
        ElementJsonGenerator $elementJsonService,
        TranslatorInterface $t,
        AsyncService $as
    ) {
        $this->elementJsonService = $elementJsonService;

        // Instantiation of FilesystemAdapter (compatible PHP 7)
        $this->cache = new FilesystemAdapter(
            'app_command_cache',                  // namespace
            3600,                                 // defaultLifetime
            __DIR__ . '/../../var/cache/commands' // explicit path for cache directory
        );

        parent::__construct($dm, $commandsLogger, $security, $t, $as);
    }

    protected function gogoConfigure(): void
    {
        $this
            ->setName('app:elements:updateJson')
            ->addArgument('ids', InputArgument::REQUIRED, 'ids to update')
            ->setDescription('Calculate again all the element JSON representations.');
    }

    protected function gogoExecute(DocumentManager $dm, InputInterface $input, OutputInterface $output): void
    {
        try {

            $currentDb = $dm->getConfiguration()->getDefaultDB();
            $currentTimestamp = time();
            $cacheKey = $currentDb . '-update_elements_json_in_progress';
            $cacheItem = $this->cache->getItem($cacheKey);
            $cacheItem->set($currentTimestamp);
            $this->cache->save($cacheItem);

            // Fetch elements based on input
            $elements = ('all' === $input->getArgument('ids'))
                ? $dm->get('Element')->findAllElements()
                : $dm->query('Element')
                    ->field('id')
                    ->in(explode(',', $input->getArgument('ids')))
                    ->execute();

            $count = $elements->count();
            $this->log('Generating JSON representation for ' . $count . ' elements...');

            $i = 0;
            foreach ($elements as $key => $element) {

                $cacheItem = $this->cache->getItem($cacheKey);
                if ($cacheItem->get() !== $currentTimestamp) {
                    $this->log('A new instance of this command is running so we stop the current one.');
                    return;
                } else {
                    $this->elementJsonService->updateJsonRepresentation($element);
                    if (0 == (++$i % 100)) {
                        $dm->flush();
                        $dm->clear();
                    }
                    if (0 == ($i % 1000)) {
                        $this->log($i . ' / ' . $count . ' elements completed...');
                    }
                }
            }

            $dm->flush();
            $dm->clear();
            $this->cache->deleteItem($cacheKey);

            $this->log('All elements successfully updated');
        } catch (\Exception $e) {
            $this->error($e->getMessage());
        }
    }

    protected function runInSeparateProcess()
    {
        return true;
    }
}
