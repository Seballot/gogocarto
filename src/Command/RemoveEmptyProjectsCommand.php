<?php

namespace App\Command;

use App\Services\DocumentManagerFactory;
use Doctrine\ODM\MongoDB\DocumentManager;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class RemoveEmptyProjectsCommand extends Command
{
    public function __construct(DocumentManagerFactory $dmFactory, LoggerInterface $commandsLogger)
    {
        $this->dmFactory = $dmFactory;
        $this->rootDm = $dmFactory->getRootManager();
        $this->mongoClient = $this->rootDm->getConnection()->getMongoClient()->getClient();
        $this->logger = $commandsLogger;
        $this->projectName = null;
        $this->output = null;
        $this->simulateOption = false;
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->setName('app:projects:delete-empty-ones')
            ->setDescription('Remove projects with no signifiant content')
            ->addArgument('project', InputArgument::REQUIRED, 'Project name or All')
            ->addArgument('nbMaxDropped', InputArgument::OPTIONAL, 'Nb max of dropped projects')
            ->addOption('simulate', null, InputOption::VALUE_NONE, 'Display counters but no real delete')
            ->addOption('force', null, InputOption::VALUE_NONE, 'Delete incomplete databases and projects');
    }

    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $this->output = $output;

        $inputProjectName = $input->getArgument('project');
        $nbMaxDropped = $input->getArgument('nbMaxDropped');
        $this->simulateOption = $input->getOption('simulate');
        $forceOption = $input->getOption('force');
        $verboseOption = $input->getOption('verbose');

        $nbProjects = 0;
        $nbActive = 0;
        $nbSpecificDB = 0;
        $nbDropped = 0;
        $nbNoProject = 0;
        $nbNoDatabase = 0;
        $nbNoConfig = 0;

        $this->log('*** Starting Remove projects with no signifiant content ***', false, true);
        if ($this->simulateOption) {
            $this->log('>>> SIMULATE MODE <<<', false, true);
        }
        
        $databases = $this->mongoClient->listDatabaseNames();
        $databaseNamesArray = iterator_to_array($databases);
        $projects = $this->rootDm->get('Project')->findAll();
        $projectNamesArray = array_map(function($project) { return $project->getDomainName(); }, $projects);
        $projectNames = array_merge($databaseNamesArray, $projectNamesArray);
        $projectNames = array_unique($projectNames);

        if ($inputProjectName !== 'all') {
            if (in_array($inputProjectName, $projectNames)) {
                $projectNames = [$inputProjectName];
            } else {
                $this->log('ABORTED : no project or database found with name ' . $inputProjectName, false, true);
                return;
            }
        }

        foreach ($projectNames as $projectName) {

            if ($projectName === null) {
                continue;
            }

            $nbProjects++;

            if ($nbMaxDropped && $nbDropped >= $nbMaxDropped) {
                $this->log('*** Nb max of dropped projects reached ***', false, true);
                break;
            }

            $this->projectName = $projectName;
            $dm = $this->dmFactory->createForDB($projectName);

            if (in_array($projectName, ['admin', 'config', 'local', 'gogocarto_default'])) {
                $nbSpecificDB++;
                $this->log('==> No action for specific database ' . $projectName, false);
                continue;
            }

            // Check if database exists
            if (in_array($projectName, $databaseNamesArray)) {

                // Check if projects exists
                if (in_array($projectName, $projectNamesArray)) {

                    $nbConfigurations = count($dm->get('Configuration')->findAll());
                    $nbElements = count($dm->get('Element')->findAll());
                    $nbUsers = count($dm->get('User')->findAll());

                    $lastLoginUser = $dm->query('User')->sort('lastLogin', 'DESC')->getOne();
                    $lastLogin = null;
                    if ($lastLoginUser && $lastLoginUser->getLastLogin()) {
                        $lastLogin = $lastLoginUser->getLastLogin();
                    }
                    $lastLoginIsOlderThanOneYearAgo = false;
                    if ($lastLogin) {
                        $currentDate = new \DateTime();
                        $oneYearAgo = $currentDate->sub(new \DateInterval('P1Y'));
                        if ($lastLogin < $oneYearAgo) {
                            $lastLoginIsOlderThanOneYearAgo = true;
                        }
                    }

                    if ($nbConfigurations > 0) {
                        if ($nbElements == 0 && ($nbUsers == 0 || $lastLoginIsOlderThanOneYearAgo)) {
                            $this->dropDatabase($projectName);
                            $this->dropProject($projectName);
                            $nbDropped++;
                            $this->log('Remove project and database with no Element and no user (or last login older than 1 year ago).');
                        } else {
                            $nbActive++;
                            if ($verboseOption) {
                                $this->log('is active => '
                                    . $nbConfigurations . ' config '
                                    . $nbElements . ' elements and '
                                    . $nbUsers . ' users. '
                                    . ($lastLogin ? 'Last login is ' . $lastLogin->format('Y-m-d H:i:s') . '.' : '')
                                );
                            }
                        }
                    } else {
                        $nbNoConfig++;
                        $message = '';
                        if ($input->getOption('force')) {
                            $this->dropDatabase($projectName);
                            $this->dropProject($projectName);
                            $nbDropped++;
                            $message = ', project and database dropped.';
                        }
                        $this->log('No Configuration found' . $message);
                    }
                } else {
                    $nbNoProject++;
                    $this->log('No project found');
                }

            } else {
                $nbNoDatabase++;
                $message = '';
                if ($input->getOption('force')) {
                    $this->dropProject($projectName);
                    $nbDropped++;
                    $message = ', project dropped.';
                }
                $this->log('No Database found' . $message);
            }
        }

        $this->log('*** Ending Remove projects with no signifiant content ***', false, true);
        $this->log($nbProjects . ' project(s)', false);
        $this->log($nbActive . ' active project(s)', false);
        $this->log($nbSpecificDB . ' untouched specific databases', false);
        $this->log($nbDropped . ' project(s) dropped', false);
        $this->log($nbNoProject . ' database(s) without project', false);
        $this->log($nbNoDatabase . ' project(s) without database' . ($forceOption ? ' (dropped)' : ''), false);
        $this->log($nbNoConfig . ' database(s) without Configuration' . ($forceOption ? ' (dropped)' : ''), false);
    }

    private function dropDatabase($projectName)
    {
        if (!$this->simulateOption) {
            $db = $this->mongoClient->selectDatabase($projectName);
            $db->command(['dropDatabase' => 1]);
        }
    }

    private function dropProject($projectName)
    {
        if (!$this->simulateOption) {
            $project = $this->rootDm->get('Project')->findOneByDomainName($projectName);
            if ($project) {
                $this->rootDm->remove($project);
                $this->rootDm->flush();
            }
        }
    }

    private function log($message, $usePrefix = true, $addABox = false)
    {
        if ($usePrefix) $message = "Project {$this->projectName} : $message";
        $this->logger->info($message);
        if ($addABox) { $this->output->writeln('***********************************************************'); }
        $this->output->writeln($message);
        if ($addABox) { $this->output->writeln('***********************************************************'); }
    }
}
