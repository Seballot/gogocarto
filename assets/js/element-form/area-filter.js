
window.areaFilterApp = {

  isFirstLoading: true,
  frenchAdministrativeBoundariesIsLoading: false,
  frenchCounties: null,
  frenchStates: null,

  // Needed to process french epecis
  processGeoApiGouvList: (dataName, apiType) => {
    if (areaFilterApp.frenchAdministrativeBoundariesIsLoading) {
      return;
    }
    const url = 'https://geo.api.gouv.fr/' + apiType;
    if (!areaFilterApp[dataName]) {
      areaFilterApp.frenchAdministrativeBoundariesIsLoading = true;
      fetch(url, { method: 'get', headers: { 'Content-Type': 'application/json' } })
      .then(response => {
        if (!response.ok) { throw new Error(url + ' : Network response was not ok'); }
        return response.json();
      })
      .then(json => {
        areaFilterApp[dataName] = json;
        areaFilterApp.frenchAdministrativeBoundariesIsLoading = false;
      })
    }
  },
  // Needed to process french epecis
  loadFrenchAdministrativeBoundaries: () => {
    if (areaFilterApp.frenchAdministrativeBoundariesIsLoading) {
      return;
    }
    areaFilterApp.processGeoApiGouvList('frenchCounties', 'departements');
    areaFilterApp.processGeoApiGouvList('frenchStates', 'regions');
  },


  searchCommune: (mode, fieldName, id, name, meta, postCode) => {

    fetch(
      'https://geo.api.gouv.fr/communes?nom=' + name + (postCode ? '&codePostal=' + postCode : ''),
      {
        method: 'get',
        headers: { 'Content-Type': 'application/json' }
      })
    .then(response => {
      if (!response.ok) {
        throw new Error('Network response was not ok');
      }
      return response.json()
    })
    .then(results => {
      if (results.length > 0 && results[0]?.codeEpci) {
        const codeEpci = results[0].codeEpci;
  
        let select2AreaContainerCssClass = null;
        switch (mode) {
          case 'selection': select2AreaContainerCssClass = '#field-' + fieldName + ' .select2-area-results-container[data-id="' + id + '"]'; break;
          case 'result': select2AreaContainerCssClass = '#select2-select-' + fieldName + '-results .select2-area-results-container[data-id="' + id + '"]';
        }
        if (!select2AreaContainerCssClass) {
          return false;
        }

        fetch('https://geo.api.gouv.fr/epcis?code=' + codeEpci, {
            method: 'get',
            headers: {
              'Content-Type': 'application/json'
            }
          })
        .then(response => {
          if (!response.ok) {
            throw new Error('Network response was not ok');
          }
          return response.json()
        })
        .then(results => {
          if (results.length > 0 && results[0].nom) {
            const epci = results[0].nom;
            $(select2AreaContainerCssClass).attr('title', name + '\n' + meta + '\n' + epci);
            if (mode === 'result') {
              $(select2AreaContainerCssClass).append( `<div>${epci}</div>` );
            }
            if (mode === 'selection') {
              const selectEncodedResultCSSClass = '#field-' + fieldName + ' .select-encoded-result';
              const selectEncodedResults = JSON.parse($(selectEncodedResultCSSClass).val());
              const selectEncodedResult = JSON.parse(selectEncodedResults[id]);
              selectEncodedResult.codeEpci = codeEpci;
              selectEncodedResult.epci = epci;
              selectEncodedResults[id] = JSON.stringify(selectEncodedResult);
              $(selectEncodedResultCSSClass).val(JSON.stringify(selectEncodedResults));
            }
          }
        })
      }
    })
      
  },


  searchEpci: (mode, fieldName, id, name, meta) => {
    
    fetch('https://geo.api.gouv.fr/epcis?nom=' + name, {
        method: 'get',
        headers: {
          'Content-Type': 'application/json'
        }
      })
    .then(response => {
      if (!response.ok) {
        throw new Error('Network response was not ok');
      }
      return response.json()
    })
    .then(results => {
      let select2AreaContainerCssClass = null;
      switch (mode) {
        case 'selection': select2AreaContainerCssClass = '#field-' + fieldName + ' .select2-area-results-container[data-id="' + id + '"]'; break;
        case 'result': select2AreaContainerCssClass = '#select2-select-' + fieldName + '-results .select2-area-results-container[data-id="' + id + '"]';
      }
      if (!select2AreaContainerCssClass) {
        return false;
      }

      if (results.length > 0) {
        const epci = results[0];
        const codeEpci = epci?.code;
        const codesDepartements = epci?.codesDepartements;
        const departements = codesDepartements.map(codeDepartement => {
          return areaFilterApp.frenchCounties.find(frenchCounty => frenchCounty.code === codeDepartement).nom;
        })
        const codesRegions = epci?.codesRegions;
        const regions = codesRegions.map(codeRegion => {
          return areaFilterApp.frenchStates.find(frenchState => frenchState.code === codeRegion).nom;
        })
        $(select2AreaContainerCssClass).attr('title', name + '\n' + meta + ' - ' + departements.join(', '));
        $(select2AreaContainerCssClass).find('.select2-area-results-meta').text( meta + ' - ' + departements.join(', ') );
        if (mode === 'selection') {
          const selectEncodedResultCSSClass = '#field-' + fieldName + ' .select-encoded-result';
          const selectEncodedResults = JSON.parse($(selectEncodedResultCSSClass).val());
          const selectEncodedResult = JSON.parse(selectEncodedResults[id]);
          if (codeEpci) {
            selectEncodedResult.codeEpci = codeEpci;
          }
          if (codesDepartements) {
            selectEncodedResult.codesDepartements = codesDepartements;
            selectEncodedResult.departements = departements;
          }
          if (codesRegions) {
            selectEncodedResult.codesDepartements = codesRegions;
            selectEncodedResult.regions = regions;
          }
          selectEncodedResults[id] = JSON.stringify(selectEncodedResult);
          $(selectEncodedResultCSSClass).val(JSON.stringify(selectEncodedResults));
        }
      }
    })
      
  },


  select2AreaTemplate: (mode, fieldName, {id, text}) => {

    if (!text) {
        return ''; // Handle case where data is not available yet
    }
    if (!text.startsWith('{')) {
      return text;
    }

    areaFilterApp.loadFrenchAdministrativeBoundaries();

    const json = JSON.parse(text);
    let { name, type, country, state, county, postCode, epci, codeEpci, departements, codesDepartements } = json;
    let meta = '';
    if (type !== 'country') {
      meta = country ?? '';
      if (state) { meta = meta + ' - ' + state ?? ''; }
      if (county) { meta = meta + ' - ' + county ?? ''; }
      if (departements) { meta = meta + ' - ' + departements.join(', ') ?? ''; }
      // Code to get data on EPCI, specific to France
      if (country === 'France' && !areaFilterApp.isFirstLoading) {
        if (type === 'city' && !codeEpci) {
          areaFilterApp.searchCommune(mode, fieldName, id, name, meta, postCode);
        }
        if (type === 'locality' && !codeEpci) {
          let formatedName = name
            .replace('Communauté de communes', 'CC')
            .replace('Communauté d\'agglomération', 'CA');
          areaFilterApp.searchEpci(mode, fieldName, id, formatedName, meta);
        }
      }
    }
    $return = $(`
      <div
        class="select2-area-results-container"
        data-id="${id}"
        data-type="${type}"
        data-name="${name}"
        data-meta="${meta}"
        title="${name}&#013;${meta}"
      >
        <div class="select2-area-results-name">${name}</div>
        <div class="select2-area-results-meta">${meta}</div>
      </div>
    `);
    if (mode === 'selection' && codeEpci) {
      $return.attr('title', name + '\n' + meta + '\n' + epci);
    }
    return $return;
  },


  select2AreaTemplateResult: (fieldName) => (data) => {
    return areaFilterApp.select2AreaTemplate('result', fieldName, data);
  },


  select2AreaTemplateSelection: (fieldName, placeholder) => (data) => {
    if (!data.id) {
      data.text = placeholder;
    }
    return areaFilterApp.select2AreaTemplate('selection', fieldName, data);
  },


  ajax: (fieldName, geocodingBounds, geocodingBoundsByCountryCodes) => {
    return {
      url: 'https://photon.komoot.io/api?layer=city&layer=county&layer=state&layer=country&layer=locality&layer=district&layer=other',
      dataType: 'json',
      cache:true,
      delay: 500,
      data: function (params) {
        let searchTerm = params.term;
        if (params.term.toLowerCase().startsWith('cc ')) { searchTerm = ('Communauté de communes ' + params.term.slice(3)); }
        if (params.term.toLowerCase().startsWith('ca ')) { searchTerm = ('Communauté d\'agglomération ' + params.term.slice(3)); }
        let dataResponse = { 
          // trick to find more easily specific french level "Communauté de communes".
          q: searchTerm,
          limit: 20,
          lang: 'fr' // supported languages are : default, en, de, fr
        }
        if (geocodingBounds) {
          dataResponse.bbox = `${geocodingBounds[1][1]},${geocodingBounds[1][0]},${geocodingBounds[0][1]},${geocodingBounds[0][0]}`;
        }
        return dataResponse;
      },
      processResults: function (data, params) {
        try {
            areaFilterApp.isFirstLoading = false;

            let features = data.features;
            // filtering osm entities
            features = features.filter(feature => {
                return (feature.properties.type !== 'locality' || feature.properties.osm_value === 'local_authority')
            })
            features = features.filter(feature => {
                return (feature.properties.type !== 'city' || feature.properties.osm_key === 'place')
            })
            features = features.filter(feature => {
              return (feature.properties.type !== 'district' || feature.properties.osm_value === 'city')
            })
            // filtering current selection
            let currentSelectedItems = $('#field-' + fieldName + ' .select2-area-results-container').toArray();
            currentSelectedItems = currentSelectedItems.map(currentSelectedItem => currentSelectedItem.dataset.id);
            features = features.filter(feature => {
                return (!currentSelectedItems.includes(feature.properties.osm_id.toString()));
            })
            // filter by country codes is not possible directly by requesting, so we filter the response
            if (geocodingBoundsByCountryCodes) {
              geocodingBoundsByCountryCodesArray = geocodingBoundsByCountryCodes.split(',');
              features = features.filter(feature => {
                  return feature?.properties?.countrycode && geocodingBoundsByCountryCodesArray.includes(feature.properties.countrycode.toLowerCase())
              })
            }

            let result = [];
            features.forEach(feature => {
                result.push({
                    id: feature.properties.osm_id,
                    text: JSON.stringify({
                        name: feature.properties.name,
                        type: feature.properties.type,
                        county: feature.properties.county,
                        state: feature.properties.state,
                        country: feature.properties.type !== 'country' ? feature.properties.country : undefined,
                        postCode: feature.properties?.postcode?.slice(0,5),
                    }),
                    title: ''
                });
            });
            return { results: result };
        } catch (error) {
            return { results: [] }; // Return an empty array in case of error
        }
      },
    }
  }

}

